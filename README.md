Goal: Monitor e-Bikes using a customized e-Bike monitoring kit comprising of a smartphone (Samsung Galaxy S-III), Phidget battery sensors, and other custom-built hardware.

Our software stack running on the phone, interfaces with the different sensors to provide data collection and durability on our university servers. In addition, it provides bike theft prevention and detection services, and automatic software update.
Organized into two primary Android Applications:

SensorDC: App that interfaces with the different smartphone sensors (via Android APIs), and the e-bike battery monitoring Phidget sensors — charging current, voltage, and battery temperature (over USB with the smartphone being the USB host/master). It collects data and stores it on flash storage. Time-series records are batched together to form a chunk. Chunks are compressed and stored in a circular datalog (see Figure). Data is uploaded periodically to a storage server.

Wobistdu: App that runs as an “Android system application”. It manages Wifi connectivity to campus Wifi, interfaces with a Update service to update the SensorDC application, and parses incoming SMS messages for a secret code, and responds to specific codes with the current or last known location of the e-bike. Additionally, this app dynamally configures the smartphone’s Wifi and GPS location radios.