#!/bin/sh

### DORoot v3.2, using 2.2.1 script ###

#### Root Install/Download Function ####
rootinstall () {
	# Checks for Platform
	Platform=$(uname)

	# Linux set of commands. Downloads everything to home directory.
	if  [ "$Platform" = "Linux" ]; then
		clear
		echo "You are on Linux. Real men use Windows!"
		sleep 3
		echo "Downloading necessary files!"
		echo " "
		echo "Downloading 1 of 1"
		wget "http://www.kimete.com/droid/beta/DoRootLinux_221.zip"
		if [ ! -e DoRootLinux_221.zip ]; then
			echo "Download failed"
			echo "Exiting program."
			sleep 5
			exit 
		fi
		echo " "
		echo "Download Complete."
		echo "Extracting files"
		unzip DoRootLinux_221.zip
		if [ ! -d DoRootLinux ]; then
            		echo "Extraction failed"
    			echo "Exiting program."
    			sleep 5
    			exit
		fi
		echo "Extraction complete."
		echo "Downloading program files to proper location."
		cd DoRootLinux

		if [ ! -e doroot.sh ]; then
			echo "No doroot.sh present. Fail."
			echo "Exiting program."
			sleep 5
			exit 
		fi

		clear

		echo "Download and file set-up complete."
		echo "Would you like to begin rooting your phone?"
		echo "Choose and option: [1 or 2]."
		echo "  "
		echo "1. Yes. I'd like DO and FLU to take over the world!!"
		echo "2. No. Screw them and and to hell with this root process."
		cheese=0
		read cheese
		if [ $cheese -eq 1 ]; then
			osch=2
		elif [ $cheese -eq 2 ]; then
			clear
			echo "DarkOnion: Screw you!!"
			echo "Time for you to go into an infinite loop!"
			echo "5"
			sleep 1
			echo "4"
			sleep 1
			echo "3"
			sleep 1
			echo "2"
			sleep 1
			echo "1"
			sleep 1
		fi
	# Mac platform and commands.
	elif [ "$Platform" = "Darwin" ]; then
		clear
		echo "You are on Mac. You fail! :P"
		sleep 3
		echo "Downloading DoRoot for Mac!"
		echo " "
		echo "Downloading 1 of 1"
		curl "http://www.kimete.com/droid/beta/DoRootMac_221.zip" > DoRootMac_221.zip
		if [ ! -e DoRootMac_221.zip ]; then
			echo "Download of files failed"
			echo "Exiting program."
			sleep 5
			exit 
		fi
		echo " "
		echo "Download Complete. Extracting files."
		unzip DoRootMac_221.zip
		echo "Extraction completed."
		echo "Downloading program files to proper location."
		cd DoRootMac

		if [ ! -e doroot.sh ]; then
			echo "doroot.sh not found. Fail."
			echo "Exiting program."
			sleep 5
			exit 
		fi

		clear
		echo "Download and file set-up complete."
		echo "Would you like to begin rooting your phone?"
		echo "Choose and option: [1 or 2]."
		echo "  "
		echo "1. Yes. I'd like DO and FLU to take over the world!!"
		echo "2. No. Screw them and and to hell with this root process!"
		cheese=0
		read cheese
		if [ $cheese -eq 1 ]; then
			osch=2
		elif [ $cheese -eq 2 ]; then
			clear
			echo "DarkOnion: 'Screw you!!'"
			echo "Time for you to go into an infinite loop in...'"
			echo "5"
			sleep 1
			echo "4"
			sleep 1
			echo "3"
			sleep 1
			echo "2"
			sleep 1
			echo "1"
			sleep 1
		fi
	else
		echo "We can not identify your operating system."
		echo "Will exit in 5 seconds."
		sleep 5
		exit 1
	fi
}
#### End of download and install ####

#### Rooting Procedure ####
root () {

	echo "The goggles! They do nothing!"
	sleep 3
	Platform=$(uname)
	if  [ "$Platform" = "Linux" ]; then
		clear
		echo "You are running Linux."
		cd ~/DoRootLinux
		echo "Negative ghostrider. The pattern is full!"
	elif  [ "$Platform" = "Darwin" ]; then
		clear
		echo "You are using that Steve Jobs' OS!"
		cd ~/DoRootMac
		echo "Be a man! Do the right thing! Use a real operating system!"
	fi
	sleep 5
	clear
	echo " "
	echo "You chose to begin rooting your phone."
	echo "We will begin rooting shortly."
	sleep 5
	clear
	# Calling commands here!
	chmod 0755 doroot.sh
	./doroot.sh
	sleep 5
	exit
}
#### End of Rooting Procedure ####


#### Unroot procedure ####

unroot () {

	clear
	echo "I luuuuuuuuv Cheeeeeeese!"
	sleep 3
	Platform=$(uname)
	if  [ "$Platform" = "Linux" ]; then
		clear
		echo "You are running Linux."
		cd ~/DoRootLinux
		echo "Negative ghostrider. The pattern is full!"
	elif  [ "$Platform" = "Darwin" ]; then
		clear
		echo "You are using that Steve Jobs' OS!"
		cd ~/DoRootMac
		echo "Be a man! Do the right thing! Use a real operating system!"
	fi
	echo " "
	echo "You have chosen to unroot your phone."
	echo "Are you sure you want to unroot your phone?"
	echo " "
	echo "1. Yes. I am trading my Droid X or Droid 2 in for a more inferior phone."
	echo "2. No. WTH is this?! Motorola Droid X/2 FTMFW!!!"
	cowpoop=0
	read cowpoop
	if [ $cowpoop = 1 ]; then
		# Call unroot here.
		echo "Unroot will begin shortly."
		sleep 5
		chmod 0755 doroot.sh
		./doroot.sh unroot
		sleep 5
		exit
	elif [ $cowpoop = 2 ]; then
		echo " "
		echo "Good man (or woman)! You did the right thing!"
		sleep 5
	fi
}
#### End of Unrooting ####


#### Credits ####
credits () {
	clear
	echo " "
	echo "Credits"
	echo "======="
	echo "DORoot ver 3.0 is presented by DarkOnion and Facelessuser."
	echo "Procedure was developed by Karnovaran."
	echo "Exploit binary developed by Sebastian Krahmer."
	echo "  "
	echo "All information in regards to DORoot can be found"
	echo "on XDA Developers forum under Droid 2 Development."
	echo "  "
	echo "If you like and/or appreciate our work, please feel"
	echo "to donate to us via PayPal or through the donate link"
	echo "on the XDA Developer thread."
	echo "DarkOnion - darkonion@kimete.com"
	echo "Facelessuser - faceless.donate@gmail.com"
	echo " "
	sleep 30
}
#### End of Credits ####


#### User interface ####
cd ~
clear

osch=0
fudge=0

echo "Welcome to DORoot ver. 3.0!"
echo " "
echo "==========================================="
echo "The Fine Print"
echo " "
echo "There is no gurantee with his program."
echo "You run it at your own risk. We can not"
echo "be held responsible if you do not get the"
echo "results you desire."
echo "==========================================="
echo " "
echo "This is presented to you by DarkOnion and Facelessuser."
echo " "
echo "Please choose and option by typing a number and pressing enter."
echo " "
echo "1. Full Install"
echo "   Downloads necessary files and roots."
echo "2. Root Process Only"
echo "3. Unroot"
echo "4. Credits and Donation Information"
echo "5. Exit"
echo " "
echo -n "Please make a selection: [1 2 3 4 or 5]"
read osch

### Reads User input ###
if [ $osch -eq 1 ] ; then
	rootinstall
fi 

# Rooting process.
if [ $osch -eq 2 ] ; then
	root	
fi

# Unroot
if [ $osch -eq 3 ] ; then
	unroot
fi

# Credits
if [ $osch -eq 4 ] ; then
	credits
fi

# Exit
if [ $osch -eq 5 ] ; then
	clear
	echo " "
	echo "Have a nice day!"
	sleep 5
	exit 1
fi


# Stupid Easter Eggs!
if [ $osch -eq 69 ] ; then
	clear
	echo " "
	echo "69! You pervert! Now I will punish you!"
	sleep 3
	fudge=1
	if [ $fudge -eq 1 ]; then
		osch=6
	fi
fi

if [ $osch -eq 6 ] ; then
	clear
	echo " "
	echo "6. There is no number 6."
	sleep 3
	if [ $fudge -eq 1 ]; then
		osch=8
	fi
fi

if [ $osch -eq 8 ] ; then
	clear
	echo " "
	echo "8. Infinite sign but up and down!"
	sleep 3
	if [ $fudge -eq 1 ]; then
		osch=12
	fi
fi

if [ $osch -eq 12 ] ; then
	clear
	echo " "
	echo "12. The amount of money currently in DarkOnion's bank account."
	sleep 3
	if [ $fudge -eq 1 ]; then
		osch=13
	fi
fi
if [ $osch -eq 13 ] ; then
	clear
	echo " "
	echo "13. DarkOnion's soccer number when he was a shrimp."
	sleep 3
	if [ $fudge -eq 1 ]; then
		osch=18
	fi
fi

if [ $osch -eq 18 ] ; then
	clear
	echo " "
	echo "18. the age that all U.S. male citizens need to sign up for selective services."
	sleep 3
	if [ $fudge -eq 1 ]; then
		osch=21
	fi
fi

if [ $osch -eq 21 ] ; then
	clear
	echo " "
	echo "21. can finally buy alcohol instead of stealing it!"
	if [ $fudge -eq 1 ]; then
		osch=33
		sleep 3
	fi
fi
