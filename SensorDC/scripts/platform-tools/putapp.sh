#!/bin/bash

apk=~/Documents/workspace/Wobistdu/bin/Wobistdu.apk
filename=Wobistdu.apk


./adb push $apk /sdcard/
./adb shell su -c mount -o rw,remount -t yaffs2 /dev/block/mtdblock3 /system
./adb shell su -c cp /sdcard/$filename /system/app/$filename
./adb shell su -c chmod 644 /system/app/$filename
./adb shell rm /sdcard/$filename


