#!/bin/gnuplot

set datafile separator ":"
 set timefmt '%Y-%m-%d %H:%M:%S'
set xdata time

plot "./data-clean.log" using 1:6 with linespoints skip 600

set term png
set out 'phonebattery.png'
replot
