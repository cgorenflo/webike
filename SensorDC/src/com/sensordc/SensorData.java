package com.sensordc;

public class SensorData {

	private int versionCode; 
	private double lat_gps = 0, long_gps = 0;
	private double lat_net = 0, long_net = 0;

	private float maccx = 0, maccy = 0, maccz = 0;
	private float magx = 0, magy = 0, magz = 0;
	private float gyrx = 0, gyry = 0, gyrz = 0;
	private float mpressure = 0, light = 0, proximity = 0;
	private float gravity = (float) 0.00;
	private float linaccx = 0, linaccy = 0, linaccz = 0;
	private int msteps = 0;

	private String significantMotionTS;

	private double phidgettemperature = Double.MIN_VALUE;
	private double phidgetambienttemperature = Double.MIN_VALUE;
	private double phidgetvoltage = Double.MIN_VALUE;
	private double phidgetcurrent = Double.MIN_VALUE;
	private double phidgetdischargecurrent = Double.MIN_VALUE;

	private String ipaddresses = "";
	private String phonebatterystatus = "";

	public SensorData(int versionCode, double lat_gps, double long_gps, double lat_net,
			double long_net, float maccx, float maccy, float maccz, float magx,
			float magy, float magz, float gyrx, float gyry, float gyrz,
			float mpressure, float light, float proximity, float gravity,
			float linaccx, float linaccy, float linaccz, int msteps,
			double phidtemperature,double phidgetambienttemperature,  double phidgetvoltage,
			double phidgetcurrent, String significantMotionTS,
			String ipaddresses, String phonebatterystatus, double phidgetdischargecurrent) {
		this.versionCode=versionCode;
		this.lat_gps = lat_gps;
		this.long_gps = long_gps;
		this.lat_net = lat_net;
		this.long_net = long_net;

		this.maccx = maccx;
		this.maccy = maccy;
		this.maccz = maccz;

		this.magx = magx;
		this.magy = magy;
		this.magz = magz;

		this.gyrx = gyrx;
		this.gyry = gyry;
		this.gyrz = gyrz;

		this.mpressure = mpressure;
		this.light = light;
		this.proximity = proximity;
		this.gravity = gravity;
		this.linaccx = linaccx;
		this.linaccy = linaccy;
		this.linaccz = linaccz;
		this.msteps = msteps;

		this.phidgettemperature = phidtemperature;
		this.phidgetambienttemperature=phidgetambienttemperature;
		this.phidgetvoltage = phidgetvoltage;
		this.phidgetcurrent = phidgetcurrent;
		this.phidgetdischargecurrent = phidgetdischargecurrent;

		this.significantMotionTS = significantMotionTS;
		this.ipaddresses = ipaddresses;
		this.phonebatterystatus = phonebatterystatus;
	}

	public String toString() {
		return versionCode+","+lat_gps + "," + long_gps + "," + lat_net + "," + long_net + ","
				+ maccx + "," + maccy + "," + maccz + "," + magx + "," + magy
				+ "," + magz + "," + gyrx + "," + gyry + "," + gyrz + ","
				+ mpressure + "," + light + "," + gravity + "," + linaccx + ","
				+ linaccy + "," + linaccz + "," + msteps + ","
				+ phidgettemperature + ","+phidgetambienttemperature+"," + phidgetvoltage + ","
				+ phidgetcurrent + "," + significantMotionTS + "," + proximity
				+ "," + ipaddresses+","+phonebatterystatus+","+phidgetdischargecurrent;
	}

}
