package com.sensordc;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.phidgets.InterfaceKitPhidget;
import com.phidgets.PhidgetException;
import com.phidgets.TemperatureSensorPhidget;
import com.phidgets.event.AttachEvent;
import com.phidgets.event.AttachListener;
import com.phidgets.event.DetachEvent;
import com.phidgets.event.DetachListener;
import com.phidgets.event.SensorChangeEvent;
import com.phidgets.event.SensorChangeListener;

public class SensorDCManager extends BroadcastReceiver implements SensorEventListener {

	
	
	private static String TAG = "sensordcmanager";
	private SensorManager _sensorManager;
	LocationManager locationManager;
	TemperatureSensorPhidget device;
	InterfaceKitPhidget interfacekit;

	private int versionCode=0;
	private double lat_gps = Double.NaN, long_gps = Double.NaN;
	private double lat_net = Double.NaN, long_net = Double.NaN;

	private float maccx = Float.NaN, maccy = Float.NaN, maccz = Float.NaN;
	private float magx = Float.NaN, magy = Float.NaN, magz = Float.NaN;
	private float gyrx = Float.NaN, gyry = Float.NaN, gyrz = Float.NaN;
	private float mpressure = Float.NaN, light = Float.NaN, proximity = Float.NaN;
	private float gravity =Float.NaN;
	private float linaccx = Float.NaN, linaccy = Float.NaN, linaccz = Float.NaN;
	private int msteps = 0;

	private double phidgettemperature = Double.NaN;
	private double phidgetambienttemperature = Double.NaN;
	//private TriggerEventListener mTriggerEventListener_SM;
	private double phidgetvoltage = -Double.NaN;
	private double phidgetcurrent = -Double.NaN;

	private String significantMotionTS = "";
	private String ipaddresses = "";
	private String phoneBatteryStatus="";

	private static long minTime_loc_gps /* in ms */= 30000;
	private static float minDistance_loc_gps /* in m */= (float) 10.00;

	private static long minTime_loc_net /* in ms */= 30000;
	private static float minDistance_loc_net /* in m */= (float) 10.00;

	private Sensor s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13;
	private Context context;
	private LocationListener locationListener_gps, locationListener_net;
	private AttachListener phidgetattachlistener;
	private DetachListener phidgetdetachlistener;
	private SensorChangeListener phidgetchangelistener;
	
	public SensorDCManager()
	{}

	public void Initialize(Context ctx)
	{
		 //_sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);;
		 locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);;
		updateVersion();
		prepareGPS(locationManager);
		
		s1 = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		s2 = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		s3 = _sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		s4 = _sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
		s5 = _sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		s6 = _sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
		s7 = _sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		s8 = _sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
//		s9 = _sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
//		s10 = _sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

		s11 = _sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
//		s12 = _sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
//		s13 = _sensorManager
//				.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
		
		
		

//		mTriggerEventListener_SM = new TriggerEventListener() {
//			@Override
//			public void onTrigger(TriggerEvent event) {
//				significantMotionTS = getCurrentTimestamp();
//				_sensorManager.requestTriggerSensor(mTriggerEventListener_SM,
//						s9);
//			}
//		};
		

		boolean acc = _sensorManager.registerListener(this, _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		SensorDCLog.e(TAG, "acc reg  is "+acc, this.getClass());
		
		_sensorManager.registerListener(this, s2,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s3,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s4,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s5,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s6,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s7,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s8,
				SensorManager.SENSOR_DELAY_NORMAL);
//		_sensorManager.requestTriggerSensor(mTriggerEventListener_SM, s9);
		_sensorManager.registerListener(this, s10,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s11,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s12,
				SensorManager.SENSOR_DELAY_NORMAL);
		_sensorManager.registerListener(this, s13,
				SensorManager.SENSOR_DELAY_NORMAL);
		
		//initPhidget();
		
	}
	
	public void Uninitialize()
	{
		try 
		{
		_sensorManager.unregisterListener(this);
		locationManager.removeUpdates(locationListener_net);
		locationManager.removeUpdates(locationListener_gps);

		//interfacekit.removeAttachListener(phidgetattachlistener);
		//interfacekit.removeDetachListener(phidgetdetachlistener);
		//interfacekit.removeSensorChangeListener(phidgetchangelistener);
		//interfacekit.close();
		//com.phidgets.usb.Manager.Uninitialize();
		}
		catch(Exception e)
		{
			SensorDCLog.e(TAG," Uninitialize "+e, this.getClass());
		}
		
	}
	
		public static String getCurrentTimestamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.S");
		return dateFormat.format(new Date());
	}
	
	public int updateVersion()
	{
		try
		{
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			versionCode= pInfo.versionCode;
			return versionCode;
			
		}
		catch(Exception e)
		{
			SensorDCLog.e(TAG," getVersion "+e, this.getClass());
			return 0;
		}
	}

	public SensorData getData() {
		getIpAddresses();
		getBatteryStatus();
		SensorData retVal = new SensorData(versionCode, lat_gps, long_gps, lat_net,
				long_net, maccx, maccy, maccz, magx, magy, magz, gyrx, gyry,
				gyrz, mpressure, light, proximity, gravity, linaccx, linaccy,
				linaccz, msteps, phidgettemperature,phidgetambienttemperature, phidgetvoltage,
				phidgetcurrent, significantMotionTS, ipaddresses,phoneBatteryStatus);

		return retVal;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	
	private boolean logged = false; 
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			maccx = event.values[0];
			maccy = event.values[1];
			maccz = event.values[2];
			
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			magx = event.values[0];
			magy = event.values[1];
			magz = event.values[2];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
			gyrx = event.values[0];
			gyry = event.values[1];
			gyrz = event.values[2];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
			mpressure = event.values[0];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
			light = event.values[0];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
			proximity = event.values[0];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
			gravity = event.values[0];
			return;
		}

		if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
			linaccx = event.values[0];
			linaccy = event.values[1];
			linaccz = event.values[2];
			return;
		}

//		if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
//			msteps = (int) event.values[0];
//			return;
//		}
		Log.i("hell", "event val = "+event.values[0]);
	}

	private void initPhidget() {
		try {
			com.phidgets.usb.Manager.Initialize(context);
			
			//Interface kit (voltage, temperature sensor) stuff
			interfacekit = new InterfaceKitPhidget();
			phidgetattachlistener = new AttachListener() {
				public void attached(final AttachEvent ae) {
					PhidgetInterfaceKitAttachDetachRunnable handler = new PhidgetInterfaceKitAttachDetachRunnable(ae.getSource(), true);
					synchronized(handler)
					{
						handler.run();
					}
				}
			};
			
			phidgetdetachlistener=new DetachListener() {
				public void detached(final DetachEvent ae) {
					PhidgetInterfaceKitAttachDetachRunnable handler = new PhidgetInterfaceKitAttachDetachRunnable(ae.getSource(), false);
					synchronized(handler)
					{
						phidgetcurrent=Double.NaN;
						phidgetvoltage=Double.NaN;
						phidgetambienttemperature = Double.NaN;
						phidgettemperature = Double.NaN;
						
						handler.run();
						//runOnUiThread(handler);
//						try {
//							handler.wait();
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}
					}
				}
			};
			
			phidgetchangelistener = new SensorChangeListener() {
				public void sensorChanged(SensorChangeEvent se) {
					
					int index = se.getIndex();
					int value = se.getValue();
					try 
					{
						// set to finest granularity
						interfacekit.setSensorChangeTrigger(index, 1);
					}
					catch(Exception e)
					{
						SensorDCLog.i(TAG, "phidget sensorChanged setSensorChangedTrigger to 1 for index "+index+" "+e, this.getClass());
					}
					
					if(index==0)
						phidgetcurrent=value;
					if(index==1)
						phidgetvoltage=(((float)value/200.00) - 2.5 ) / (0.0681);
					if(index==2)
						phidgetambienttemperature = interpolateTemperature (value, MainActivity.t1ambient, MainActivity.t2ambient, MainActivity.v1ambient, MainActivity.v2ambient );
					if(index==3)
						phidgettemperature=interpolateTemperature(value,MainActivity.t1battery, MainActivity.t2battery, MainActivity.v1battery, MainActivity.v2battery );
						
				}
			};
			
			interfacekit.addAttachListener(phidgetattachlistener);
			interfacekit.addDetachListener(phidgetdetachlistener);
			interfacekit.addSensorChangeListener(phidgetchangelistener);
			
			
			
			// Temperature sensor stuff
//			device = new TemperatureSensorPhidget();
//			
//			
//			device.addAttachListener(new AttachListener() {
//				public void attached(final AttachEvent attachEvent) {
//					PhidgetAttachEventHandler handler = new PhidgetAttachEventHandler(
//							attachEvent.getSource());
//					// This is synchronised in case more than one device is
//					// attached before one completes attaching
//					synchronized (handler) {
//						handler.run();
//					}
//				}
//			});
//
//			device.addDetachListener(new DetachListener() {
//				public void detached(final DetachEvent detachEvent) {
//					PhidgetDetachEventHandler handler = new PhidgetDetachEventHandler(
//							detachEvent.getSource());
//					// This is synchronised in case more than one device is
//					// attached before one completes attaching
//					synchronized (handler) {
//						phidgettemperature = Double.NaN;
//						phidgetambienttemperature = Double.NaN;
//						// runOnUiThread(handler);
//						handler.run();
////						try {
////						//	handler.wait();
////						} catch (InterruptedException e) {
////							e.printStackTrace();
////						}
//					}
//				}
//			});
//
//			device.addTemperatureChangeListener(new TemperatureChangeListener() {
//				public void temperatureChanged(final TemperatureChangeEvent oe) {
//					try {
//						Handler refresh = new Handler(Looper.getMainLooper());
//						refresh.post(new Runnable() {
//							public void run() {
//								//@rayman, removing now because temperature sensors go 
//								// directly into interface kit 
//						//		phidgettemperature = oe.getValue();
//							}
//						});
//						//phidgetambienttemperature=device.getAmbientTemperature();
//					} catch (Exception e) {
//						SensorDCLog.e("phidget ",""+ e, this.getClass());
//					}
//				}
//			});
//			
//		
//			
//			
//			BroadcastReceiver receiver = new BroadcastReceiver() {
//				int i=1;
//				   @Override
//				   public void onReceive(Context context, Intent intent) {
//				      UsbDevice d = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
//				        
//				      
//				     UsbManager m = (UsbManager) context.getSystemService(context.USB_SERVICE);
//				     PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent("com.android.example.USB_PERMISSION"), 0);
//				     
//				     m.requestPermission(d,mPermissionIntent );
//				      
//				      try
//			    	  {
//				    	  SensorDCLog.e(TAG, "phidget receiver "+intent.getAction()+"," + d.getDeviceName() + ","+d.getDeviceId()+",vendorid:"+d.getVendorId()+",prod id: "+d.getProductId() +","+d.getInterface(0).getId(), this.getClass());
//				    	  if(i==1)
//				    	  {
//				    		//  device.openAny();
//				    	//  interfacekit.openAny();
//				    	  	i++;
//				    	  }
//				    	 
//				    	  
//			    	  }
//			    	  catch (Exception e)
//			    	  {
//			    		  SensorDCLog.e(TAG, "phidget receiver exception "+e, this.getClass());
//			    	  }
//				      
//				      
//				      
//				   }
//				};
//			
				//IntentFilter filter = new IntentFilter();
				//filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
				
				//context.registerReceiver(receiver, filter);
			//	device.open(-1);
				interfacekit.open(-1);  
			
			
		} catch (PhidgetException e) {
			SensorDCLog.e(TAG, "initPhidget " + e, this.getClass());
		} catch (Exception e) {
			SensorDCLog.e(TAG, "initPhidget " + e, this.getClass());
		}

	}
	
	// Milad's temperature interpolation
	private float interpolateTemperature(int val, float t1, float t2, float v1, float v2)
	{
		float a = (t1 - t2) / (v1 - v2);
		float b = (t2*v1 - t1*v2) / (v1 - v2);
		return a*val + b ; 
	}

	private void prepareGPS(LocationManager locationManager) {
		
		locationListener_gps = new LocationListener() {
			public void onLocationChanged(Location location) {
				lat_gps = location.getLatitude();
				long_gps = location.getLongitude();
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		locationListener_net = new LocationListener() {
			public void onLocationChanged(Location location) {
				lat_net = location.getLatitude();
				long_net = location.getLongitude();
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				minTime_loc_gps, minDistance_loc_gps, locationListener_gps);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, minTime_loc_net,
				minDistance_loc_net, locationListener_net);
		
	}

	private void getIpAddresses() {
		try {
			ipaddresses = "";
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
						ipaddresses = inetAddress.getHostAddress().toString()+";"+ipaddresses ;
					}
				}
			}
		} catch (Exception e) {
			ipaddresses="";
			SensorDCLog.e(TAG, " getIpAddresses " + e, this.getClass());
		}

	}

	private void getBatteryStatus()
	{
		try
		{
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = context.registerReceiver(null, ifilter);
		int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
		boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
		                     status == BatteryManager.BATTERY_STATUS_FULL;

		// How are we charging?
		int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
		boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
		
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		float batteryPct = level / (float)scale;
		phoneBatteryStatus=isCharging+":"+usbCharge+":"+acCharge+":"+batteryPct;
		}
		catch(Exception e)
		{
			phoneBatteryStatus="";
			SensorDCLog.e(TAG," getBatteryStatus "+e, this.getClass());
		}
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		 PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	     PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
	     wl.acquire();
	     this.context = context;
	     SensorManager _sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
	     s1 = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	     _sensorManager.registerListener(this, s1, SensorManager.SENSOR_DELAY_FASTEST);

	     //getIpAddresses();
	     //getBatteryStatus();
	  //   this.Initialize(context);
	     
	     
	     SystemClock.sleep(1000); 
	    	
	  //  this.Uninitialize();
	     
	     //wl.release();
		
	}
	
}
